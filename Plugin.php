<?php

namespace Kanboard\Plugin\mmi;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'mmImporter';
    }

    public function getPluginDescription()
    {
        return t('Mindmap importer as project/swimline/task/subtask');
    }

    public function getPluginAuthor()
    {
        return 'opouy';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://gricad-gitlab.univ-grenoble-alpes.fr/publickanboarddev/mmimporter';
    }
}

